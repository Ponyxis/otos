// SPDX-License-Identifier: GPL-3.0-only
#pragma once

#include <Arduino.h>
#include <SPI.h>
#include "RA8875.h"

class Display{
    public:
        Display();
        void Init(uint16_t textColor);
        void write(const char* format, ...);
        void render();
        void fillScreen(uint16_t color);
        void removeCharacter();
    private:
        RA8875 tft = RA8875(10, 9); //CS, RESET
        static const uint8_t MAX_LINE_LENGTH = 100;
        static const uint8_t MAX_X = 100;
        static const uint8_t MAX_Y = 31;

        char lcd_buff[MAX_Y][MAX_X] = { 0 };
        uint8_t lcd_line_size[MAX_Y + 1] = { 0 };
        uint8_t actualX = 0;
        uint8_t actualY = 0;
        void _scrollDown();
        

};

extern Display LCD;
