// SPDX-License-Identifier: GPL-3.0-only
#include "input_parser.hpp"
#include "display.hpp"

InputParser::InputParser() {
}

void InputParser::process(char in) {
    switch (in) {
        case 0x08: // backspace
            if (linePosition > 0) {
                _removeCharacter();
                LCD.removeCharacter();
            }
            break;
        case 0x0D: // enter
            LCD.write("\n");
            LCD.write(">>");
            _clearLine();
            break;
        default:
            LCD.write("%c", in);
            inputLine[linePosition] = in;
            linePosition++;
            break;
    }
}

void InputParser::_clearLine() {
    memset(inputLine, 0x00, MAX_LINE_LEN);
    linePosition = 0;
}

void InputParser::_removeCharacter() {
    inputLine[linePosition - 1] = 0x00;
    linePosition--;
}
