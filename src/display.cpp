// SPDX-License-Identifier: GPL-3.0-only
#include "display.hpp"

Display LCD;

Display::Display() {
}

void Display::Init(uint16_t textColor) {
    tft.begin(RA8875_800x480, 8, 27000000);
    tft.setTextColor(textColor);
    tft.showCursor(IBEAM, true);
    tft.setCursorBlinkRate(100);
}

void Display::write(const char* format, ...) {
    va_list args;
    char buffer[MAX_LINE_LENGTH];

    va_start(args,format);
    uint8_t size = vsprintf (buffer, format, args );
    va_end(args);

    for (uint8_t i = 0; i < size; i++) {
        switch (buffer[i]) {
            case '\n':
                lcd_buff[actualY][actualX] = '\n';
                actualY++;
                actualX = 0;
                break;
            
            default:
                lcd_buff[actualY][actualX] = buffer[i];
                lcd_line_size[actualY]++;
                actualX++;
                break;
        }
    }

    if (actualY == MAX_Y-1) {
        _scrollDown();
        actualY = actualY - 1;
    }

}

void Display::render() {
    tft.fillScreen(RA8875_BLACK);
    tft.setCursor(0, 0);

    for (uint8_t y = 0; y <= actualY; y++) {
        for (uint8_t x = 0; x <= lcd_line_size[y]; x++) {
            if (lcd_buff[y][x] == '\n') {
                tft.println();
            } else if (lcd_buff[y][x] != 0) {
                tft.printf("%c", lcd_buff[y][x]);
            }
        }
    }
}

void Display::fillScreen(uint16_t color) {
    tft.fillScreen(color);
}

void Display::removeCharacter() {
    lcd_buff[actualY][actualX - 1] = '\0';
    lcd_line_size[actualY]--;
    actualX--;
}

void Display::_scrollDown() {
    memset(lcd_buff[0], 0x00, MAX_X - 1);
    lcd_line_size[0] = 0;

    for (uint8_t lines = 0; lines < MAX_Y; lines++) {
        strncpy(lcd_buff[lines], lcd_buff[lines + 1], MAX_X - 1);
        lcd_line_size[lines] = lcd_line_size[lines + 1];
    }

    memset(lcd_buff[MAX_Y - 1], 0x00, MAX_X - 1);
    lcd_line_size[MAX_Y - 1] = 0;
}
