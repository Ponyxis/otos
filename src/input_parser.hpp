// SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include "inttypes.h"

class InputParser {
public:
    InputParser();
    void process(char in);
private:
    static const uint8_t MAX_LINE_LEN = 254;
    char inputLine[MAX_LINE_LEN] = { '\0' };
    uint8_t linePosition = 0;
    void _clearLine();
    void _removeCharacter();
};
