// SPDX-License-Identifier: GPL-3.0-only
#include <Arduino.h>
#include "display.hpp"
#include "input_parser.hpp"

char incomingData = '\0';

InputParser inputParser;

void setup() {
    Serial8.begin(115200); // Arduino 13x5 keypad
    LCD.Init(RA8875_GREEN);
    LCD.write("OpenTeensyOS v0.0.1\n>>");
    LCD.render();
}

void loop() {
    if (Serial8.available()) {
        incomingData = Serial8.read();
        inputParser.process(incomingData);
        LCD.render();
    }
    yield();
}